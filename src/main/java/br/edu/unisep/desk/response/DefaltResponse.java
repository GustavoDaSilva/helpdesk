package br.edu.unisep.desk.response;

import lombok.Getter;

@Getter
public class DefaltResponse<T> {

    private final String message;
    private final T result;

    private DefaltResponse(String message, T result) {
        this.message = message;
        this.result = result;
    }

    private DefaltResponse(T result) {
        this("", result);
    }

    public static <R> DefaltResponse<R> of(R result) {
        return new DefaltResponse<>(result);
    }

    public static <R> DefaltResponse<R> of(String message, R result) {
        return new DefaltResponse<>(message, result);
    }

}
