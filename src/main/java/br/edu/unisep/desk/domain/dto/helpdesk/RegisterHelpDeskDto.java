package br.edu.unisep.desk.domain.dto.helpdesk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterHelpDeskDto {

    private String username;

    private String email;

    private String title;

    private String nameResponder;

    private Integer status;

    private Date cadastro;

    private Date fechamento;

    private String emailResponder;

    private String descricao;;
}



