package br.edu.unisep.desk.domain.usecase.helpdesk;

import br.edu.unisep.desk.data.repository.HelpDeskRepository;
import br.edu.unisep.desk.domain.builder.helpdesk.HelpDeskBuilder;
import br.edu.unisep.desk.domain.dto.helpdesk.HelpDeskDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindResponsavelHelpDesk {
    private final HelpDeskRepository helpDeskRepository;
    private final HelpDeskBuilder builder;

    public List<HelpDeskDto> execute(String nameResponder) {
        var desks = helpDeskRepository.findByResponseContainingIgnoreCase(nameResponder);
        return builder.from(desks);
    }
}
