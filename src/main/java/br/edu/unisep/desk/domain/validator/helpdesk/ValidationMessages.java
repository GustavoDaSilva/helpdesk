package br.edu.unisep.desk.domain.validator.helpdesk;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationMessages {
    public static final String MESSAGE_REQUIRED_DESK_USERNAME = "Informe o nome do usuário!";
    public static final String MESSAGE_REQUIRED_DESK_EMAIL = "Informe o email de usuário válido!";
    public static final String MESSAGE_REQUIRED_DESK_TITLE = "Informe o titulo do ticket!";
    public static final String MESSAGE_REQUIRED_DESK_RESPONDER= "Informe o nome do responsável pelo ticket!";
    public static final String MESSAGE_REQUIRED_DESK_DESCRICAO = "Informe a descrição!";
    public static final String MESSAGE_REQUIRED_DESK_EMAILRESPONDER = "Informe o email do responsáve válido!";
}
