package br.edu.unisep.desk.exception;

import br.edu.unisep.desk.response.DefaltResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationExceptionHandler {
    @ExceptionHandler({IllegalArgumentException.class, NullPointerException.class})
    public ResponseEntity<DefaltResponse<Boolean>> handleValidationErrors(Exception exception) {
        return ResponseEntity.badRequest().body(DefaltResponse.of(exception.getMessage(), false));
    }
}
